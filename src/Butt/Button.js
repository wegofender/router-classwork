import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Button extends Component {
  render() {
    const {text, value} = this.props
    return (
      <button >
        {text}
        {value}
      </button>
    );
  }
}

Button.defaultProps = {
  text: 'abc',
  value: 5
};

Button.propTypes = {
  text: PropTypes.string,
  value: PropTypes.number.isRequired
};


export default Button;


